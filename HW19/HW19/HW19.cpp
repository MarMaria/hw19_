﻿// HW19.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include "pch.h"
#include <iostream>
#include <string>

class Animal
{
private:
	Animal() {}
	std::string type;
	std::string name;
	std::string sound;

protected:
	void SetSound(std::string _sound)
	{
		sound = _sound;
	}

public:
	Animal(std::string _type) : type(_type)
	{}
	Animal(std::string _type, std::string _name) : type(_type), name(_name)
	{}

	virtual void Voice()
	{
		std::cout << sound << '\n';
	}

	std::string GetType()
	{
		return type;
	}

	std::string GetName()
	{
		return name;
	}
};

class Cat : public Animal
{
public:
	Cat() : Animal("Cat")
	{}

	Cat(std::string _name) : Animal("Cat", _name)
	{}

	void Voice() override
	{
		std::cout << "Nya!" << '\n';
	}
};

class Dog : public Animal
{
public:
	Dog() : Animal("Dog")
	{}

	Dog(std::string _name) : Animal("Dog", _name)
	{
		SetSound("Woof!");
	}
};

class Cockroach : public Animal
{
public:
	Cockroach() : Animal("Cockroach")
	{}

	Cockroach(std::string _name) :Animal("Cockroach", _name)
	{}

	void Voice() override
	{
		std::cout << "Nothing" << '\n';
	}
};



int main()
{
	Cat cat1("Alice");
	Cat cat2("Melissa");
	Dog dog1("Tecci");
	Cockroach cockroach1("NoName");

	Animal* myPets[] = { &cat1, &cat2, &dog1, &cockroach1 };

	for (auto pet : myPets)
	{
		std::cout << "My " << pet->GetType() << " " << pet->GetName() << " says: ";
		pet->Voice();
	}
}

// Запуск программы: CTRL+F5 или меню "Отладка" > "Запуск без отладки"
// Отладка программы: F5 или меню "Отладка" > "Запустить отладку"

// Советы по началу работы 
//   1. В окне обозревателя решений можно добавлять файлы и управлять ими.
//   2. В окне Team Explorer можно подключиться к системе управления версиями.
//   3. В окне "Выходные данные" можно просматривать выходные данные сборки и другие сообщения.
//   4. В окне "Список ошибок" можно просматривать ошибки.
//   5. Последовательно выберите пункты меню "Проект" > "Добавить новый элемент", чтобы создать файлы кода, или "Проект" > "Добавить существующий элемент", чтобы добавить в проект существующие файлы кода.
//   6. Чтобы снова открыть этот проект позже, выберите пункты меню "Файл" > "Открыть" > "Проект" и выберите SLN-файл.
